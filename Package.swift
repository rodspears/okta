// swift-tools-version:5.2
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "Okta",
    products: [
        // Products define the executables and libraries produced by a package, and make them visible to other packages.
        .library(
            name: "Okta",
            targets: ["OktaOidc"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages which this package depends on.
        .target(
           name: "OktaUserAgent", // 1
           dependencies: [], // 2
           exclude: ["OktaOidc", "AppAuth"], // 4
           cSettings: [
              .headerSearchPath("Internal"), // 5
           ]
        ),
        .target(
           name: "AppAuth", // 1
           dependencies: ["OktaUserAgent"], // 2
           exclude: ["OktaOidc"] // 4
//           cSettings: [
//              .headerSearchPath("Internal"), // 5
//           ]
        ),
        .target(
           name: "OktaOidc", // 6
           dependencies: ["OktaUserAgent"] // 7
        ),
//        .testTarget(
//            name: "OktaTests",
//            dependencies: ["Okta"]),
    ]
)
