import XCTest
@testable import Okta

final class OktaTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(Okta().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
