import XCTest

import OktaTests

var tests = [XCTestCaseEntry]()
tests += OktaTests.allTests()
XCTMain(tests)
